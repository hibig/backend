# Merge Request

<!--
Thanks for contributing to our project!
Before creating a merge request, please read this first:

- Use the template below
- Use a clear and descriptive title for the issue to identify the merge request
- Create the merge request as early as possible and mark it as a draft to show others that you are working on it
  https://docs.gitlab.com/ee/user/project/merge_requests/drafts.html
- Once you remove the draft label, your merge request will be considered for the review process
- You can merge the merge request once you have approval from at least one developer. If you are not authorized to do so, please wait for a maintainer to merge the request for you.
- After submitting your merge request, verify that the pipeline passes
- Link to the issue you are woking on

-->

## Breaking Changes

<!--
Description of how the API behaves differently now.
If you have the permission, please also add the label "Breaking Change" to this merge request.
-->

## Description

<!--
This was done
-->

### Checklist

- [ ] The code compiles without any warnings.
- [ ] I followed the [code review checklist](https://gitlab.com/dlr-shepard/backend/-/blob/master/CONTRIBUTING.md#code-review-checklist).
- [ ] The documentation has been added/updated.
- [ ] There are no `System.out.println()` statements.

### Related Issues

- Related #<issue number>
- Closes #<issue number>
