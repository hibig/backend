package de.dlr.shepard.neo4Core.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.dlr.shepard.BaseTestCase;
import nl.jqno.equalsverifier.EqualsVerifier;

public class CollectionTest extends BaseTestCase {

	@Test
	public void equalsContract() {
		EqualsVerifier.simple().forClass(Collection.class)
				.withPrefabValues(DataObject.class, new DataObject(1L), new DataObject(2L))
				.withPrefabValues(User.class, new User("bob"), new User("claus"))
				.withPrefabValues(CollectionReference.class, new CollectionReference(1L), new CollectionReference(2L))
				.verify();
	}

	@Test
	public void addDataObjectTest() {
		var col = new Collection(1L);
		var dataObject = new DataObject(2L);
		col.addDataObject(dataObject);

		assertEquals(col.getDataObjects(), List.of(dataObject));
	}

}
