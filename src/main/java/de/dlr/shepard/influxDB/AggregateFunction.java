package de.dlr.shepard.influxDB;

public enum AggregateFunction {
	MEAN, MEDIAN, COUNT, SUM, MIN, MAX
}
