package de.dlr.shepard.filters;

import java.io.IOException;

import de.dlr.shepard.exceptions.ApiError;
import de.dlr.shepard.exceptions.InvalidPathException;
import de.dlr.shepard.neo4Core.services.UrlPathChecker;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;

@Provider
@Slf4j
public class UrlPathCheckerFilter implements ContainerRequestFilter {

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		var urlPathChecker = getUrlPathChecker();

		try {
			urlPathChecker.checkPathSegments(requestContext.getUriInfo().getPathSegments());
		} catch (InvalidPathException e) {
			log.warn("Cought invalid path exception: {}", e.getMessage());
			var status = Status.NOT_FOUND.getStatusCode();
			requestContext.abortWith(Response.status(status)
					.entity(new ApiError(status, e.getClass().toString(), e.getMessage())).build());
		}

	}

	protected UrlPathChecker getUrlPathChecker() {
		return new UrlPathChecker();
	}

}
