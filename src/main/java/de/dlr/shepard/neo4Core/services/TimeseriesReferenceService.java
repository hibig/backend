package de.dlr.shepard.neo4Core.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.dlr.shepard.exceptions.InvalidBodyException;
import de.dlr.shepard.influxDB.AggregateFunction;
import de.dlr.shepard.influxDB.TimeseriesPayload;
import de.dlr.shepard.influxDB.TimeseriesService;
import de.dlr.shepard.neo4Core.dao.DataObjectDAO;
import de.dlr.shepard.neo4Core.dao.TimeseriesContainerDAO;
import de.dlr.shepard.neo4Core.dao.TimeseriesReferenceDAO;
import de.dlr.shepard.neo4Core.dao.UserDAO;
import de.dlr.shepard.neo4Core.entities.TimeseriesReference;
import de.dlr.shepard.neo4Core.io.TimeseriesReferenceIO;
import de.dlr.shepard.security.PermissionsUtil;
import de.dlr.shepard.util.AccessType;
import de.dlr.shepard.util.DateHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TimeseriesReferenceService {
	private TimeseriesReferenceDAO timeseriesReferenceDAO = new TimeseriesReferenceDAO();
	private TimeseriesService timeseriesService = new TimeseriesService();
	private DataObjectDAO dataObjectDAO = new DataObjectDAO();
	private TimeseriesContainerDAO timeseriesContainerDAO = new TimeseriesContainerDAO();
	private UserDAO userDAO = new UserDAO();
	private DateHelper dateHelper = new DateHelper();
	private PermissionsUtil permissionsUtil = new PermissionsUtil();

	public List<TimeseriesReference> getAllTimeseriesReferences(long dataObjectId) {
		var references = timeseriesReferenceDAO.findByDataObject(dataObjectId);
		return references;
	}

	public TimeseriesReference getTimeseriesReference(long id) {
		var reference = timeseriesReferenceDAO.find(id);
		if (reference == null || reference.isDeleted()) {
			log.error("Timeseries Reference with id {} is null or deleted", id);
			return null;
		}
		return reference;
	}

	public TimeseriesReference createTimeseriesReference(long dataObjectId, TimeseriesReferenceIO timeseriesReference,
			String username) throws InvalidBodyException {
		var user = userDAO.find(username);
		var dataObject = dataObjectDAO.find(dataObjectId);
		var container = timeseriesContainerDAO.find(timeseriesReference.getTimeseriesContainerId());

		if (container == null || container.isDeleted()) {
			throw new InvalidBodyException(String.format("The timeseries container with id %d could not be found.",
					timeseriesReference.getTimeseriesContainerId()));
		}

		var toCreate = new TimeseriesReference();
		toCreate.setCreatedAt(dateHelper.getDate());
		toCreate.setCreatedBy(user);
		toCreate.setDataObject(dataObject);
		toCreate.setName(timeseriesReference.getName());
		toCreate.setStart(timeseriesReference.getStart());
		toCreate.setEnd(timeseriesReference.getEnd());
		toCreate.setTimeseries(Arrays.asList(timeseriesReference.getTimeseries()));
		toCreate.setTimeseriesContainer(container);

		var created = timeseriesReferenceDAO.createOrUpdate(toCreate);
		return created;
	}

	public boolean deleteTimeseriesReference(long timeseriesId, String username) {
		var user = userDAO.find(username);

		var old = timeseriesReferenceDAO.find(timeseriesId);
		old.setDeleted(true);
		old.setUpdatedAt(dateHelper.getDate());
		old.setUpdatedBy(user);

		timeseriesReferenceDAO.createOrUpdate(old);
		return true;
	}

	public List<TimeseriesPayload> getPayload(long timeseriesId, AggregateFunction function, Long groupBy,
			Set<String> devicesFilterSet, Set<String> locationsFilterSet, Set<String> symbolicNameFilterSet,
			String username) {
		var ref = timeseriesReferenceDAO.find(timeseriesId);
		var containerId = ref.getTimeseriesContainer().getId();
		var database = ref.getTimeseriesContainer().getDatabase();

		if (permissionsUtil.isAllowed(containerId, AccessType.Read, username)) {
			return timeseriesService.getTimeseriesList(ref.getStart(), ref.getEnd(), database, ref.getTimeseries(),
					function, groupBy, devicesFilterSet, locationsFilterSet, symbolicNameFilterSet);
		}
		return Collections.emptyList();
	}

	public InputStream export(long timeseriesId, AggregateFunction function, Long groupBy, Set<String> devicesFilterSet,
			Set<String> locationsFilterSet, Set<String> symbolicNameFilterSet, String username) throws IOException {
		var ref = timeseriesReferenceDAO.find(timeseriesId);
		var containerId = ref.getTimeseriesContainer().getId();
		var database = ref.getTimeseriesContainer().getDatabase();

		if (permissionsUtil.isAllowed(containerId, AccessType.Read, username)) {
			return timeseriesService.exportTimeseries(ref.getStart(), ref.getEnd(), database, ref.getTimeseries(),
					function, groupBy, devicesFilterSet, locationsFilterSet, symbolicNameFilterSet);
		}
		return null;
	}

}
