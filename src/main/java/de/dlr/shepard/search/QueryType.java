package de.dlr.shepard.search;

public enum QueryType {

	StructuredData, Collection, DataObject, Reference;

}
